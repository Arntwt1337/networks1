#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <signal.h>


#define BUF_SIZE 1024


typedef struct UDPServer {
    int socket_fd;

    struct sockaddr_in servAddr;
    struct sockaddr_in cliAddr;

} UDPServer_t;


UDPServer_t UDP_SERVER;


void Server_DisplayServerInfo() {
    printf("Информация о сервере %s : %d\n", inet_ntoa(UDP_SERVER.servAddr.sin_addr), ntohs(UDP_SERVER.servAddr.sin_port));
}

void Server_DisplayClientInfo() {
    printf("Информация о клиенте %s : %d\n", inet_ntoa(UDP_SERVER.cliAddr.sin_addr), ntohs(UDP_SERVER.cliAddr.sin_port));
}


int Server_Init() {
    
    if ((UDP_SERVER.socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        return 1;

    UDP_SERVER.servAddr.sin_family = AF_INET;
    UDP_SERVER.servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    UDP_SERVER.servAddr.sin_port = 0;

    if (bind(UDP_SERVER.socket_fd, (struct sockaddr*) &(UDP_SERVER.servAddr), sizeof UDP_SERVER.servAddr))
        return 2;

    unsigned int addrLen = sizeof UDP_SERVER.servAddr;
    if (getsockname(UDP_SERVER.socket_fd, (struct sockaddr*) &(UDP_SERVER.servAddr), &addrLen))
        return 3;

    return 0;
}


int Server_Run() {
    char buffer[BUF_SIZE];

    Server_DisplayServerInfo();

    unsigned int msgLen, addrLen;

    for (;;) {
        addrLen = sizeof UDP_SERVER.cliAddr;

        printf("\nПожалуйста ожидайте\n");

        bzero(buffer, BUF_SIZE);

        if ((msgLen = recvfrom(UDP_SERVER.socket_fd, buffer, BUF_SIZE, 0, (struct sockaddr*) &UDP_SERVER.cliAddr, &addrLen)) < 0)
            return 1;

        Server_DisplayClientInfo();

        printf("Сообщение от клиента: \'%s\'\n", buffer);

        if (sendto(UDP_SERVER.socket_fd, buffer, strlen(buffer), 0, (struct sockaddr*) &UDP_SERVER.cliAddr, sizeof UDP_SERVER.servAddr) < 0)
            return 2;
    }
}


int Server_Terminate() {
    if(UDP_SERVER.socket_fd) {
        close(UDP_SERVER.socket_fd);
        printf("\nЗакрытие сокета\n");
    }

    return 0;
}


int main() {

    int err;
    err = Server_Init();

    switch (err) {
        case 1:
            printf("Error: Cannot create a socket\n");
            return 0;

        case 2:
            printf("Error: Cannot bind a socket\n");
            return 0;

        case 3:
            printf("Error: Cannot get socket name\n");
            return 0;
    }

    err = Server_Run();

    switch (err) {
        case 1:
            printf("Error: Cannot reciev a message\n");
            return 0;

        case 2:
            printf("Error: Sending message failed\n");
            return 0;

    }

    err = Server_Terminate();

    return 0;
}





