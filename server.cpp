#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

using namespace std;

#define SIZE_OF_BUF 100

int main()
{
	int server_socket;
	struct sockaddr_in server_addr, client_addr;
	char buf[SIZE_OF_BUF];
	char send_buf[SIZE_OF_BUF] = "Hi, I'm server!";

	server_socket = socket(AF_INET, SOCK_DGRAM, 0);
	if (server_socket < 0)
	{
		cout << "ERROR: failed to open socket" << endl;
		exit(1);
	}

	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	server_addr.sin_port = 0;

	auto try_bind =
		bind(server_socket, (struct sockaddr *)&server_addr, sizeof(server_addr));

	if (try_bind == -1)
	{
		cout << "ERROR:Server binding failed" << endl;
		exit(1);
	}
	auto length = sizeof(server_addr);
	auto try_get_sock_name = getsockname(
		server_socket, (struct sockaddr *)&server_addr, (socklen_t *)&length);

	if (try_get_sock_name == -1)
	{
		cout << "ERROR: Getsockname call failed" << endl;
		exit(1);
	}

	cout << "Server: port number: " << ntohs(server_addr.sin_port) << endl;
	cout << endl;

	while (true)
	{
		auto client_length = sizeof(client_addr);
		memset(&buf, 0, SIZE_OF_BUF);
		auto try_recv = recvfrom(
			server_socket, buf, SIZE_OF_BUF, 0, (struct sockaddr *)&client_addr,
			(socklen_t *)&client_length);
		if (try_recv == -1)
		{
			cout << "ERROR: receive failed" << endl;
			exit(1);
		}

		cout << "Server: client IP address: " << endl
			 << inet_ntoa(client_addr.sin_addr) << endl;

		cout << "Server: client PORT: " << endl
			 << ntohs(client_addr.sin_port) << endl;

		cout << "Server: Client message: " << endl
			 << buf << endl
			 << endl;

		auto try_send = sendto(
			server_socket, send_buf, SIZE_OF_BUF, 0,
			(struct sockaddr *)&client_addr, client_length);
		if (try_send == -1)
		{
			cout << "ERROR: send failed" << endl;
			exit(1);
		}
	}

	return 0;
}
