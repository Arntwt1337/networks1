#include <arpa/inet.h>
#include <errno.h>
#include <iostream>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <cstdlib>
#include <stdio.h>

#define SIZE_OF_BUF 100

using namespace std;

int main(int argc, char *argv[])
{
    int sock;
    struct sockaddr_in servAddr, clientAddr;
    struct hostent *hp, *gethostbyname(const char *name);
    char buf[SIZE_OF_BUF];
    string buffer;
    bzero(buf, SIZE_OF_BUF);

    if (argc < 3)
    {
        printf("Input IP add, port\n");
        exit(1);
    }

    if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("Can't get socket'\n");
        exit(1);
    }

    bzero((char *)&servAddr, sizeof(servAddr));

    servAddr.sin_family = AF_INET;

    hp = gethostbyname(argv[1]);

    bcopy(hp->h_addr, &servAddr.sin_addr, hp->h_length);
    servAddr.sin_port = htons(atoi(argv[2]));
    bzero((char *)&clientAddr, sizeof(clientAddr));

    clientAddr.sin_family = AF_INET;
    clientAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    clientAddr.sin_port = 0;

    if (bind(sock, (struct sockaddr *)&clientAddr, sizeof(clientAddr)))
    {
        perror("Client port getting failed");
        exit(1);
    }

    for (size_t i = 0; i < 15; i++)
    {

        std::string str = std::to_string(i);

        for (size_t j = 0; j < SIZE_OF_BUF && j < str.length(); j++)
        {
            buf[j] = str[j];
        }

        sleep(i);

        if (sendto(
                sock, buf, SIZE_OF_BUF, 0, (struct sockaddr *)&servAddr,
                sizeof(servAddr)) < 0)
        {
            perror("Send error");
            exit(1);
        }
    }

    close(sock);

    return 0;
}
