#include <sys/types.h>
#include <sys/socket.h>
#include <iostream>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <strings.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#define SIZE_OF_BUF 100

using namespace std;

int main(int argc, char *argv[])
{
  int sock;
  struct sockaddr_in servAddr, clientAddr;
  struct hostent *hp, *gethostbyname(const char *name);
  char buf[SIZE_OF_BUF];

  if (argc < 4)
  {
    printf("Input udpclient, host_name, port, message \n");
    exit(1);
  }

  if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
  {
    perror("Can't get socket'\n");
    exit(1);
  }

  bzero((char *)&servAddr, sizeof(servAddr));

  servAddr.sin_family = AF_INET;

  hp = gethostbyname(argv[1]);

  bcopy(hp->h_addr, &servAddr.sin_addr, hp->h_length);
  servAddr.sin_port = htons(atoi(argv[2]));
  bzero((char *)&clientAddr, sizeof(clientAddr));

  clientAddr.sin_family = AF_INET;
  clientAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  clientAddr.sin_port = 0;

  if (bind(sock, (struct sockaddr *)&clientAddr, sizeof(clientAddr)))
  {
    perror("Client port getting failed");
    exit(1);
  }

  if (sendto(sock, argv[3], strlen(argv[3]), 0, (struct sockaddr *)&servAddr, sizeof(servAddr)) < 0)
  {
    perror("Send error");
    exit(1);
  }

  cout << "CLIENT: Succesfull send" << endl;

  auto lenght = sizeof(servAddr);
  if (recvfrom(sock, buf, SIZE_OF_BUF, 0, (struct sockaddr *)&servAddr, (socklen_t *)&lenght) < 0)
  {
    perror("Send error");
    exit(1);
  }

  cout << "Client: Server message: " << endl
       << buf << endl;

  close(sock);

  return 0;
}
